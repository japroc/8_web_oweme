from flask import Flask, request
from oweme_app import oweme
import json
from werkzeug.exceptions import BadRequest
app = Flask(__name__)

@app.route("/", methods=['GET', 'POST'])
def oweme_app():
    # Delimiting
    print('[+] ' + '-' * 50)
    print('[+] Connection address: ', request.remote_addr)

    # Get request data
    try:
        req = json.dumps(request.get_json(force=True))
    except BadRequest as e:
        req = ''
    except Exception as e:
        print(e)
        req = ''
    print('[>] Total Received (len: {}): {}'.format(len(req), req))

    # Oweme processing
    res = oweme.handleRequest(req)
    print('[<] Response: {}'.format(res))

    # Build response
    response = app.response_class(
        response=res,
        status=200,
        mimetype='application/json'
    )
    print('[+] ' + '-' * 50)

    # return "<h1 style='color:blue'>Hello There!</h1>"
    return response

# @app.route("/hello", methods=['GET', 'POST'])
# def hello():
#     return "<h1 style='color:blue'>Hello There!</h1>"

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
    