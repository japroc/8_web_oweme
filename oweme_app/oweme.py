import json
import math

from . import omdb
from .omex import *
from .omglobals import *

# ---------------
# ACTION HANDLERS
# ---------------

def handleActionRegistration(msg):
    """
    [IN]:
        (str)username
        (str)password
    """
    print('[!] handleActionRegistration {}'.format(msg))

    # Check params
    username = msg.get('username')
    omAssert(username, 'username not presented')
    omAssert(isinstance(username, str), 'username must be a string')
    password = msg.get('password')
    omAssert(password, 'password not presented')
    omAssert(isinstance(password, str), 'password must be a string')
    omAssert(len(password) > 5, 'password is too short')

    # Registration process
    user = omdb.registration(username, password)

    # Build ret dict
    res = {
        'status':ERROR_SUCCESS,
        'result':{
            'id':user['id'],
            'username':user['name']
        }
    }

    return json.dumps(res)

def handleActionLogin(msg):
    """
    [IN]:
        (str)username
        (str)password
    [OUT]:
        (str)token
    """
    print('[!] handleActionLogin {}'.format(msg))

    # Check params
    username = msg.get('username')
    omAssert(username, 'username not presented')
    omAssert(isinstance(username, str), 'username must be a string')
    password = msg.get('password')
    omAssert(password, 'password not presented')
    omAssert(isinstance(password, str), 'password must be a string')
    omAssert(len(password) > 5, 'password is too short')

    # login process
    token = omdb.login(username, password)

    res = {
        'status':ERROR_SUCCESS,
        'result':{
            'token':token,
        }
    }

    return json.dumps(res)

def handleActionLogout(msg):
    """
    [IN]:
        (str)token
    """
    print('[!] handleActionLogout {}'.format(msg))

    # Check params
    token = msg.get('token')
    omAssert(token, 'token not presented')
    omAssert(isinstance(token, str), 'token must be a string')

    # login process
    omdb.logout(token)

    res = {
        'status':ERROR_SUCCESS,
    }

    return json.dumps(res)

def handleActionCreateBook(msg):
    """
    [IN]:
        (str)token
        (str)name
    [OUT]:
        (int)id
        (str)name
        (str)owner
        (str)create_time
    """
    print('[!] handleActionCreateBook {}'.format(msg))

    # Check params
    token = msg.get('token')
    omAssert(token, 'token not presented')
    omAssert(isinstance(token, str), 'token must be a string')
    name = msg.get('name')
    omAssert(name, 'name not presented')
    omAssert(isinstance(name, str), 'name must be a string')

    # createBook process
    book = omdb.createBook(token, name)

    res = {
        'status':ERROR_SUCCESS,
        'result':{
            'id':book['id'],
            'name':book['name'],
            'owner':book['owner'],
            'create_time':book['create_time'],
        }
    }

    return json.dumps(res)

def handleActionDeleteBook(msg):
    """
    [IN]:
        (str)token
        (int)id
    """
    print('[!] handleActionDeleteBook {}'.format(msg))

    # Check params
    token = msg.get('token')
    omAssert(token, 'token not presented')
    omAssert(isinstance(token, str), 'token must be a string')
    book_id = msg.get('id')
    omAssert(book_id, 'id not presented')
    omAssert(isinstance(book_id, int), 'id must be an integer')

    # createBook process
    omdb.deleteBook(token, book_id)

    return json.dumps({'status':ERROR_SUCCESS})

def handleActionAddUserToBook(msg):
    """
    [IN]:
        (str)token
        (int)book_id
        (str)username
    """
    print('[!] handleActionAddUserToBook {}'.format(msg))

    # Check params
    token = msg.get('token')
    omAssert(token, 'token not presented')
    omAssert(isinstance(token, str), 'token must be a string')
    username = msg.get('username')
    omAssert(username, 'username not presented')
    omAssert(isinstance(username, str), 'username must be a string')
    book_id = msg.get('book_id')
    omAssert(book_id, 'book_id not presented')
    omAssert(isinstance(book_id, int), 'book_id must be an integer')

    # addUserToBook process
    omdb.addUserToBook(token, book_id, username)

    return json.dumps({'status':ERROR_SUCCESS})

def handleActionUpgradeUser(msg):
    """
    [IN]:
        (str)token
        (int)book_id
        (str)username
    """
    print('[!] handleActionUpgradeUser {}'.format(msg))

    # Check params
    token = msg.get('token')
    omAssert(token, 'token not presented')
    omAssert(isinstance(token, str), 'token must be a string')
    username = msg.get('username')
    omAssert(username, 'username not presented')
    omAssert(isinstance(username, str), 'username must be a string')
    book_id = msg.get('book_id')
    omAssert(book_id, 'book_id not presented')
    omAssert(isinstance(book_id, int), 'id must be an integer')

    # upgradeUser process
    omdb.upgradeUser(token, book_id, username)

    return json.dumps({'status':ERROR_SUCCESS})

def handleActionDowngradeUser(msg):
    """
    [IN]:
        (str)token
        (int)book_id
        (str)username
    """
    print('[!] handleActionDowngradeUser {}'.format(msg))

    # Check params
    token = msg.get('token')
    omAssert(token, 'token not presented')
    omAssert(isinstance(token, str), 'token must be a string')
    username = msg.get('username')
    omAssert(username, 'username not presented')
    omAssert(isinstance(username, str), 'username must be a string')
    book_id = msg.get('book_id')
    omAssert(book_id, 'book_id not presented')
    omAssert(isinstance(book_id, int), 'book_id must be an integer')

    # downgradeUser process
    omdb.downgradeUser(token, book_id, username)

    return json.dumps({'status':ERROR_SUCCESS})

def handleActionListBooks(msg):
    """
    [IN]:
        (str)token
    [OUT]
        [{'id':1, 'name':'book1', 'owner':'user1'}, ...]
    """
    print('[!] handleActionListBooks {}'.format(msg))

    # Check params
    token = msg.get('token')
    omAssert(token, 'token not presented')
    omAssert(isinstance(token, str), 'token must be a string')

    # process
    res = omdb.listBooks(token)

    return json.dumps({'status':ERROR_SUCCESS, 'result':res})

def handleActionGetBookInfo(msg):
    """
    [IN]:
        (str)token
        (int)book_id
    [OUT]
        {'id':1, 'name':'bookname', 'owner':'user1', 
        'users':[{'name':user1, 'is_admin':True}, 
        {'name':user2, 'is_admin':False}, ...], 'transactions':[]}
    """
    print('[!] handleActionGetBookInfo {}'.format(msg))

    # Check params
    token = msg.get('token')
    omAssert(token, 'token not presented')
    omAssert(isinstance(token, str), 'token must be a string')
    book_id = msg.get('book_id')
    omAssert(book_id, 'book_id not presented')
    omAssert(isinstance(book_id, int), 'book_id must be an integer')

    # downgradeUser process
    res = omdb.getBookInfo(token, book_id)

    return json.dumps({'status':ERROR_SUCCESS, 'result':res})

def handleActionAddTransaction(msg):
    """
    [IN]:
        (str)token
        (int)book_id
        (str)user_from
        (str)user_to
        (double)amount
    [OUT]:
        (int)id
        (int)book_id
        (str)user_from
        (str)user_to
        (double)amount
        (str)create_time
    """
    print('[!] handleActionAddTransaction {}'.format(msg))

    # Check params
    token = msg.get('token')
    omAssert(token, 'token not presented')
    omAssert(isinstance(token, str), 'token must be a string')
    book_id = msg.get('book_id')
    omAssert(book_id, 'book_id not presented')
    omAssert(isinstance(book_id, int), 'book_id must be an integer value')
    user_from = msg.get('user_from')
    omAssert(user_from, 'user_from not presented')
    omAssert(isinstance(user_from, str), 'user_from must be a string')
    user_to = msg.get('user_to')
    omAssert(user_to, 'user_to not presented')
    omAssert(isinstance(user_to, str), 'user_to must be a string')
    amount = msg.get('amount')
    omAssert(amount, 'amount not presented')
    omAssert(isinstance(amount, float) or isinstance(amount, int), 'amount must be a float or integer value')
    frac, _ = math.modf(amount)
    omAssert(len(str(frac)[2:]) <= 2, 'too much symbols after floating point')
    omAssert(amount > 0, 'amount must be greater than 0')

    # addTransaction process
    res = omdb.addTransaction(token, book_id, user_from, user_to, amount)

    return json.dumps({'status':ERROR_SUCCESS, 'result':res})

def handleActionRemoveTransaction(msg):
    """
    [IN]:
        (str)token
        (int)transaction_id
    """
    print('[!] handleActionRemoveTransaction {}'.format(msg))

    # Check params
    token = msg.get('token')
    omAssert(token, 'token not presented')
    omAssert(isinstance(token, str), 'token must be a string')
    transaction_id = msg.get('transaction_id')
    omAssert(transaction_id, 'transaction_id not presented')
    omAssert(isinstance(transaction_id, int), 'transaction_id must be an integer value')

    # removeTransaction process
    omdb.removeTransaction(token, transaction_id)

    return json.dumps({'status':ERROR_SUCCESS})

def handleActionSimplify(msg):
    """
    [IN]:
        (str)token
        (int)book_id
    """
    print('[!] handleActionAddTransaction {}'.format(msg))

    # Check params
    token = msg.get('token')
    omAssert(token, 'token not presented')
    omAssert(isinstance(token, str), 'token must be a string')
    book_id = msg.get('book_id')
    omAssert(book_id, 'book_id not presented')
    omAssert(isinstance(book_id, int), 'book_id must be an integer value')

    # addTransaction process
    res = omdb.simplify(token, book_id)

    return json.dumps({'status':ERROR_SUCCESS, 'result':res})
    
# MAIN HANDLERS
handlers = {
    ACTION_REGISTRATION: handleActionRegistration,
    ACTION_LOGIN: handleActionLogin,
    ACTION_LOGOUT: handleActionLogout,
    ACTION_CREATE_BOOK: handleActionCreateBook,
    ACTION_DELETE_BOOK: handleActionDeleteBook,
    ACTION_ADD_USER_TO_BOOK: handleActionAddUserToBook,
    ACTION_UPGRADE_USER: handleActionUpgradeUser,
    ACTION_DOWNGRADE_USER: handleActionDowngradeUser,
    ACTION_LIST_BOOKS: handleActionListBooks,
    ACTION_GET_BOOK_INFO: handleActionGetBookInfo,
    ACTION_ADD_TRANSACTION: handleActionAddTransaction,
    ACTION_REMOVE_TRANSACTION: handleActionRemoveTransaction,
    ACTION_SIMPLIFY: handleActionSimplify,
}

def mainHandler(msg):
    # Catching json decoding exceptions
    try:
        msg = json.loads(msg)
    except json.decoder.JSONDecodeError as e:
        print(e)
        return json.dumps({'status':ERROR_ERROR, 'message':'unable to decode json message'})

    # Check that msg is dict
    cond = isinstance(msg, dict)
    omAssert(cond, 'received json is not dict')

    # Check that msg has param 'action'
    action = msg.get('action')
    omAssert(action, 'no action presented')
    omAssert(action in ALL_ACTIONS, 'unknown action')

    # Call an appropriate handler
    return handlers[action](msg)

def handleRequest(request_msg):
    # Catches msg handling exceptions
    # and build and return error message
    try:
        return mainHandler(request_msg)
    except OMException as e:
        return json.dumps({'status':ERROR_ERROR, 'message':str(e)})

# Short wrapper
do = handleRequest
