import peewee as pw
import datetime
from .settings import DB_NAME

db = pw.SqliteDatabase(DB_NAME)

class OMModel(pw.Model):
    class Meta:
        database = db

# -------
# Models
# -------

class User(OMModel):
    name = pw.CharField(unique=True)
    passwd = pw.CharField()

class Session(OMModel):
    user = pw.ForeignKeyField(User)
    token = pw.CharField(unique=True)
    start_time = pw.DateTimeField(default=datetime.datetime.now)

class Book(OMModel):
    owner = pw.ForeignKeyField(User)
    name = pw.CharField()
    create_time = pw.DateTimeField(default=datetime.datetime.now)

class BookUser(OMModel):
    book = pw.ForeignKeyField(Book)
    user = pw.ForeignKeyField(User)
    is_admin = pw.BooleanField(default=False)

class Transaction(OMModel):
    book = pw.ForeignKeyField(Book)
    user_from = pw.ForeignKeyField(User)
    user_to = pw.ForeignKeyField(User)
    amount = pw.DoubleField()
    create_time = pw.DateTimeField(default=datetime.datetime.now)

""" Add new models to MODELS """
MODELS = [User, Session, Book, BookUser, Transaction]

# -------------
# Open ^ Close
# -------------

def dbOpen():
    db.connect()
    db.create_tables(MODELS)

def dbClose():
    db.close()
    