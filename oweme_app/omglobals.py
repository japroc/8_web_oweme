import json

# AUTH
ACTION_REGISTRATION = 'registration'
ACTION_LOGIN = 'login'
ACTION_LOGOUT = 'logout'
# FRIENDS
	# ACTION_ADD_FRIEND = 'add_friend'
	# ACTION_REMOVE_FRIEND = 'remove_friend'
	# ACTION_LIST_FRIENDS = 'list_friends'

# BOOKS
ACTION_CREATE_BOOK = 'create_book'
ACTION_DELETE_BOOK = 'delete_book'
ACTION_LIST_BOOKS = 'list_books'
ACTION_GET_BOOK_INFO = 'get_book_info'

# BOOK_USER
ACTION_ADD_USER_TO_BOOK = 'add_user_to_book'
ACTION_REMOVE_USER_FROM_BOOK = 'remove_user_from_book'
ACTION_UPGRADE_USER = 'upgrade_user'
ACTION_DOWNGRADE_USER = 'downgrade_user'

# TRANSACTIONS
ACTION_ADD_TRANSACTION = 'add_transaction'
ACTION_ADD_FAIR_TRANSACTION = 'add_fair_transaction'
ACTION_ADD_CHECK = 'add_check'
ACTION_REMOVE_TRANSACTION = 'remove_transaction'

# SIMPLIFICATOR
ACTION_SIMPLIFY = 'simplify'

# ALL ACTIONS LIST
ALL_ACTIONS = [
    ACTION_REGISTRATION, ACTION_LOGIN, ACTION_LOGOUT,
    # ACTION_ADD_FRIEND, ACTION_REMOVE_FRIEND, ACTION_LIST_FRIENDS,
    ACTION_CREATE_BOOK, ACTION_DELETE_BOOK, ACTION_LIST_BOOKS, ACTION_GET_BOOK_INFO,
    ACTION_ADD_USER_TO_BOOK, ACTION_REMOVE_USER_FROM_BOOK,
    ACTION_UPGRADE_USER, ACTION_DOWNGRADE_USER,
    ACTION_ADD_TRANSACTION, ACTION_ADD_FAIR_TRANSACTION,
    ACTION_ADD_CHECK, ACTION_REMOVE_TRANSACTION,
    ACTION_SIMPLIFY
]

# ERROR
ERROR_SUCCESS = 'ok'
ERROR_ERROR = 'error'

# ERROR_RESPONSE = json.dumps({'status':'error'})
