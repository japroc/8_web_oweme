
# SERVER
TCP_IP = '0.0.0.0'
TCP_PORT = 8080
BUFFER_SIZE = 1024  # Normally 1024, but we want fast response

# DATABASE
DB_NAME = 'db.sqlite3'
TEST_DB_NAME = 'test.sqlite3' # only used in tests

# AUTH
TOKEN_LENGTH = 64
