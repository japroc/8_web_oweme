class OMException(Exception):
    def __init__(self, message):
        # Call the base class constructor with the parameters it needs
        super(OMException, self).__init__(message)

def omAssert(cond, msg=''):
    """ Raise OMException if (cond is False) OR (cond is None)"""
    cond1 = cond is None
    cond2 = cond is False
    if cond1 or cond2:
        raise OMException(msg)
