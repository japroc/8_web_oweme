# TRANS = [
# 	{'from': '1', 'to': '2', 'amount': 1000},	# 0
# 	{'from': '2', 'to': '3', 'amount':  800},	# 1
# 	{'from': '3', 'to': '4', 'amount':  700},
# 	{'from': '4', 'to': '2', 'amount': 1100},
# 	{'from': '1', 'to': '5', 'amount':  100},
# 	{'from': '5', 'to': '6', 'amount':  700},
# 	{'from': '6', 'to': '4', 'amount':  500},	# len(TRANS) - 2
# 	{'from': '4', 'to': '5', 'amount':  100},	# len(TRANS) - 1
# ]

T1 = [
	{'from': '1', 'to': '2', 'amount': 700},	# 0
	{'from': '2', 'to': '3', 'amount': 800},	# 1
	{'from': '3', 'to': '1', 'amount': 700},
]

T2 = [
	{'from': '2', 'to': '4', 'amount': 100},	# 0
	{'from': '2', 'to': '3', 'amount': 230},	# 1
	{'from': '3', 'to': '4', 'amount': 120},
]

TRANS = list()

MARKED = list()

def simplifyEqualAndReversed():
	# Simplify equal and reversed from_to
	cur_idx = 0
	while cur_idx < len(TRANS) - 1:

		another_idx = len(TRANS) - 1
		while another_idx > cur_idx:

			# simplify equal from_to
			cond1 = TRANS[cur_idx]['to'] == TRANS[another_idx]['to']
			cond2 = TRANS[cur_idx]['from'] == TRANS[another_idx]['from']
			if cond1 and cond2:
				TRANS[cur_idx]['amount'] += TRANS[another_idx]['amount']
				del TRANS[another_idx]

			else:
				# simplify reversed from_to
				cond1 = TRANS[cur_idx]['from'] == TRANS[another_idx]['to']
				cond2 = TRANS[cur_idx]['to'] == TRANS[another_idx]['from']
				if cond1 and cond2:
					TRANS[cur_idx]['amount'] -= TRANS[another_idx]['amount']
					del TRANS[another_idx]

			# go to next
			another_idx -= 1

		cur_idx += 1


def clearAndReverseTrans():
	# Reverse negative amounts
	# And remove zero amounts
	cur_idx = 0
	while cur_idx < len(TRANS):

		# remove zero amounts
		if TRANS[cur_idx]['amount'] == 0:
			del TRANS[cur_idx]

		# reverse negative ones
		elif TRANS[cur_idx]['amount'] < 0:
			TRANS.append({'from': TRANS[cur_idx]['to'], 'to': TRANS[cur_idx]['from'], 'amount': -1 * TRANS[cur_idx]['amount']})
			del TRANS[cur_idx]

		# go to next if nothing to do
		else:
			cur_idx += 1


def getAllUsers():
	users = set()

	for t in TRANS:
		users.update([t['from'], t['to']])

	return list(users)


def handleCycle(cycled_elems):

	# Get edges of the cycle
	edges = [t for t in TRANS if t['from'] in cycled_elems and t['to'] in cycled_elems]

	# Get min amount and corresponsing edge
	min_edge = edges[0]
	min_amount = min_edge['amount']
	for edge in edges:
		if edge['amount'] < min_amount:
			min_edge = edge
			min_amount = min_edge['amount']

	# remove min edge
	TRANS.remove(min_edge)

	# sub other amounts
	for edge in edges:
		edge['amount'] -= min_amount

	# remove zero edges
	for edge in edges:
		if edge['amount'] == 0:
			TRANS.remove(edge)

def indexByFromAndTo(_list, _from, _to):
	for i in range(len(_list)):
		cond1 = _list[i]['from'] == _from
		cond2 = _list[i]['to'] == _to
		if cond1 and cond2:
			return i

	return -1

def handleLine(elems):
	
	# Get edges for the line
	# edges = [t for t in TRANS if t['from'] in elems and t['to'] in elems]
	edges = []
	for idx in range(len(elems) - 1):
		tidx = indexByFromAndTo(TRANS, elems[idx], elems[idx + 1])
		edges.append(TRANS[tidx])

	# Get min amount and corresponsing edge
	idx = 0
	min_idx = idx
	min_edge = edges[min_idx]
	min_amount = min_edge['amount']
	for edge in edges:
		if edge['amount'] < min_amount:
			min_idx = idx
			min_edge = edge
			min_amount = min_edge['amount']
		idx += 1

	# remove min edge
	TRANS.remove(min_edge)
	del edges[min_idx]

	# sub other amounts
	for edge in edges:
		edge['amount'] -= min_amount

	# remove zero edges
	for edge in edges:
		if edge['amount'] == 0:
			TRANS.remove(edge)
			edges.remove(edge)

	# Add new edge from first to last user
	# Or increase amount if exists
	idx = indexByFromAndTo(TRANS, elems[0], elems[-1])
	if idx != -1:
		TRANS[idx]['amount'] += min_amount
		return

	idx = indexByFromAndTo(TRANS, elems[-1], elems[0])
	if idx != -1:

		if TRANS[idx]['amount'] == min_amount:
			del TRANS[idx]
		elif TRANS[idx]['amount'] > min_amount:
			TRANS[idx]['amount'] -= min_amount
		else:
			new_edge = {'from': elems[0], 'to': elems[-1], 
				'amount': (TRANS[idx]['amount'] + min_amount)}
			del TRANS[idx]
			TRANS.append(new_edge)
		return

	# now corresponing edge in graph
	TRANS.append({'from': elems[0], 'to': elems[-1], 'amount': min_amount})


def recursiveDiscovery(cur_user):
	global MARKED
	
	edges = [t for t in TRANS if t['from'] == cur_user]

	if len(edges) == 0 and len(MARKED) > 1:
		elems = MARKED[:]
		elems.append(cur_user)
		handleLine(elems)
		return True
	# Cycles or discovery
	else:
		for edge in edges:
			# is target market already
			# that means cycle
			try:
				idx = MARKED.index(edge['to'])
				cycled_elems = MARKED[idx:]
				cycled_elems.append(cur_user)
				handleCycle(cycled_elems)
				return True
			# To elements is not marked
			except ValueError:
				MARKED.append(cur_user)
				res = recursiveDiscovery(edge['to'])
				MARKED = MARKED[:-1]
				if res:
					return True

	return False


def cycleSimplifier():

	cur_user_idx = 0
	while cur_user_idx < len(getAllUsers()):
		if not recursiveDiscovery(getAllUsers()[cur_user_idx]):
			cur_user_idx += 1


def simplify(transactions):
	global TRANS

	TRANS = transactions

	# Check that there are minimum 2 elements in TRANS
	if len(TRANS) < 2:
		return TRANS

	# Simplify equal and reversed from_to
	simplifyEqualAndReversed()

	# Reverse negative amounts
	# And remove zero amounts
	clearAndReverseTrans()
	
	# Check that remains minimum 2 elements in TRANS
	if len(TRANS) < 2:
		return TRANS

	# detect and simplify cycles
	cycleSimplifier()

	return TRANS


if __name__ == '__main__':
	print(T1)
	print(simplify(T1))
