import bcrypt
import string
import random
import peewee as pw

from .settings import TOKEN_LENGTH
from .omex import omAssert
from .omdbtables import dbOpen, dbClose
from .omdbtables import User, Session, Book, BookUser, Transaction
from . import simplifier

def registration(username, password):
    """
    [IN] (str)username, (str)password
    [OUT] 
        (dict){(int)id, (str)name}
    """
    dbOpen()

    user_query = User.select().where(User.name == username)
    # if username is in use, raise exception
    if user_query.exists():
        dbClose()
        omAssert(None, msg='username {} is in use'.format(username))

    # hash the password
    hashed_passwd = bcrypt.hashpw(password.encode(), bcrypt.gensalt())
    # create new User entry in db
    user = User(name=username, passwd=hashed_passwd)
    user.save()

    dbClose()

    # return some info about user
    return {
        'id':user.id,
        'name':user.name,
    }

def login(username, password):
    """
    [IN]: 
        (str)username, (str)password
    [OUT]: 
        (str)token
    """
    def genToken():
        alpha = string.ascii_uppercase + string.digits + string.ascii_lowercase
        return ''.join(random.choice(alpha) for _ in range(TOKEN_LENGTH))

    dbOpen()

    try:
        user = User.get(User.name == username)

        if not bcrypt.checkpw(password.encode(), user.passwd.encode()):
            dbClose()
            omAssert(None, msg='password is incorrect')
        else:
            token = genToken()
            session = Session(user=user, token=token)
            session.save()

            dbClose()
            return token

    # If user not found - Raise exception
    except pw.DoesNotExist:
        dbClose()
        omAssert(None, msg='user {} not found'.format(username))

def logout(token):
    """
    [IN]: 
        (str)token
    """
    dbOpen()

    try:
        session = Session.get(Session.token == token)
        session.delete_instance()

        dbClose()
        return

    # If user not found - Raise exception
    except pw.DoesNotExist:
        dbClose()
        omAssert(None, msg='token {} not found'.format(token))

def createBook(token, name):
    """
    [IN] (str)token, (str)name
    [OUT] 
        (dict){(int)id, (str)name, (str)owner, (str)create_time}
    """
    dbOpen()

    try:
        # get user
        session = Session.get(Session.token == token)
        user = session.user

        # create book
        book = Book(name=name, owner=user)
        book.save()
        bookuser = BookUser(user=user, book=book, is_admin=True)
        bookuser.save()

        dbClose()
        return {
            'id':book.id,
            'name':name,
            'owner':user.name,
            'create_time':str(book.create_time)
        }

    # If user not found - Raise exception
    except pw.DoesNotExist:
        dbClose()
        omAssert(None, msg='token {} not found'.format(token))

def deleteBook(token, book_id):
    """
    [IN] (str)token, (int)book_id
    """
    dbOpen()

    try:
        # get user
        book = Book.get(Book.id == book_id)
        session = Session.get(Session.token == token)
        user = session.user
        bookuser = BookUser.get(BookUser.user == user, BookUser.book == book)

        if bookuser.is_admin is False:
            dbClose()
            omAssert(None, msg='user must be an admin to delete book')

        book = Book.get(Book.id == book_id)
        book.delete_instance(recursive=True)

        dbClose()
        return

    # If book or session not found - Raise exception
    except Book.DoesNotExist:
        dbClose()
        omAssert(None, msg='book with id {} not found'.format(book_id))
    except Session.DoesNotExist:
        dbClose()
        omAssert(None, msg='token {} not found'.format(token))

def addUserToBook(token, book_id, username):
    """
    [IN] 
        (str)token, 
        (int)book_id
        (str)username
    """
    dbOpen()

    try:
        book = Book.get(Book.id == book_id)
        session = Session.get(Session.token == token)
        caller = session.user
        bookuser_caller = BookUser.get(BookUser.user == caller, BookUser.book == book)

        # check that caller is admin of the book
        if bookuser_caller.is_admin is False:
            dbClose()
            omAssert(None, msg='caller must be an admin to perform action')

        user = User.get(User.name == username)

        # check that user is not in book
        if BookUser.select().where(BookUser.user == user, BookUser.book == book).count() != 0:
            dbClose()
            omAssert(None, msg='user {} already is in book id {}'.format(username, book_id))

        # adding user to book
        bookuser = BookUser(user=user, book=book)
        bookuser.save()

        dbClose()
        return

    # If user, book or session not found - Raise exception
    except Book.DoesNotExist:
        dbClose()
        omAssert(None, msg='book with id {} not found'.format(book_id))
    except Session.DoesNotExist:
        dbClose()
        omAssert(None, msg='token {} not found'.format(token))
    except User.DoesNotExist:
        dbClose()
        omAssert(None, msg='user {} not found'.format(username))
    except BookUser.DoesNotExist:
        name = caller.name
        dbClose()
        omAssert(None, msg='caller {} not in book id {}'.format(name, book_id))

def upgradeUser(token, book_id, username):
    """
    [IN] 
        (str)token, 
        (int)book_id
        (str)username
    """
    dbOpen()

    try:
        book = Book.get(Book.id == book_id)
        session = Session.get(Session.token == token)
        caller = session.user
        bookuser_caller = BookUser.get(BookUser.user == caller, BookUser.book == book)

        # check that caller is admin of the book
        if bookuser_caller.is_admin is False:
            dbClose()
            omAssert(None, msg='caller must be an admin to perform action')

        # check that username is in book
        user = User.get(User.name == username)
        try:
            bookuser = BookUser.get(BookUser.user == user, BookUser.book == book)
        except BookUser.DoesNotExist:
            dbClose()
            omAssert(None, msg='user {} not in book id {}'.format(username, book_id))

        # check that user is not an admin
        if bookuser.is_admin is True:
            dbClose()
            omAssert(None, msg='user {} already is admin in book id {}'.format(username, book_id))

        # make username admin
        bookuser.is_admin = True
        bookuser.save()

        dbClose()
        return

    # If user, book or session not found - Raise exception
    except Book.DoesNotExist:
        dbClose()
        omAssert(None, msg='book with id {} not found'.format(book_id))
    except Session.DoesNotExist:
        dbClose()
        omAssert(None, msg='token {} not found'.format(token))
    except User.DoesNotExist:
        dbClose()
        omAssert(None, msg='user {} not found'.format(username))
    except BookUser.DoesNotExist:
        name = caller.name
        dbClose()
        omAssert(None, msg='caller {} not in book id {}'.format(name, book_id))

def downgradeUser(token, book_id, username):
    """
    [IN] 
        (str)token, 
        (int)book_id
        (str)username
    """
    dbOpen()

    try:
        book = Book.get(Book.id == book_id)
        session = Session.get(Session.token == token)
        caller = session.user
        bookuser_caller = BookUser.get(BookUser.user == caller, BookUser.book == book)

        # check that caller is admin of the book
        if bookuser_caller.is_admin is False:
            dbClose()
            omAssert(None, msg='caller must be an admin to perform action')

        user = User.get(User.name == username)

        # check that username is in book
        try:
            bookuser = BookUser.get(BookUser.user == user, BookUser.book == book)
        except BookUser.DoesNotExist:
            dbClose()
            omAssert(None, msg='user {} not in book id {}'.format(username, book_id))

        # check that user is an admin
        if bookuser.is_admin is False:
            dbClose()
            omAssert(None, msg='user {} is not admin in book id {}'.format(username, book_id))

        # downgrading
        bookuser.is_admin = False
        bookuser.save()

        dbClose()
        return

    # If user, book or session not found - Raise exception
    except Book.DoesNotExist:
        dbClose()
        omAssert(None, msg='book with id {} not found'.format(book_id))
    except Session.DoesNotExist:
        dbClose()
        omAssert(None, msg='token {} not found'.format(token))
    except User.DoesNotExist:
        dbClose()
        omAssert(None, msg='user {} not found'.format(username))
    except BookUser.DoesNotExist:
        name = caller.name
        dbClose()
        omAssert(None, msg='caller {} not in book id {}'.format(name, book_id))

def listBooks(token):
    """
    [IN] 
        (str)token, 
    """
    dbOpen()

    # If session not found - Raise exception
    try:
        session = Session.get(Session.token == token)
    except Session.DoesNotExist:
        dbClose()
        omAssert(None, msg='unknown token: {}'.format(token))

    user = session.user
    query = (Book
            .select(Book.id, Book.name, Book.owner)
            .join(BookUser)
            .where(BookUser.user == user))

    books = [{'id':book.id, 'name':book.name, 'owner':book.owner.name} for book in query]

    dbClose()
    return books

def getBookInfo(token, book_id):
    """
    [IN]:
        (str)token, 
        (int)book_id
    [OUT]:
        {'id':1, 'name':'bookname', 'owner':'user1', 
        'users':[{'name':user1, 'is_admin':True}, 
        {'name':user2, 'is_admin':False}, ...], 
        'transactions':[{'from': 'user1', 'to': 'user2', 'amount': 100.0}, ...]
    """
    dbOpen()

    try:
        session = Session.get(Session.token == token)
        user = session.user
        book = Book.get(Book.id == book_id)

        # check that user is in book
        BookUser.get(BookUser.user == user, BookUser.book == book)

        query = (BookUser
                .select(BookUser.user, BookUser.is_admin)
                .join(User)
                .where(BookUser.book == book))

        query_trans = Transaction.select().where(Transaction.book == book)

        users = [{'name':bookuser.user.name, 'is_admin':bookuser.is_admin} for bookuser in query]
        transactions = [{'from': trans.user_from.name, 'to': trans.user_to.name,
                        'amount': trans.amount} for trans in query_trans]
        res = {
            'id':book_id,
            'name':book.name,
            'owner':book.owner.name,
            'users':users,
            'transactions':transactions
        }

        dbClose()
        return res

    # If book or session not found - Raise exception
    except Book.DoesNotExist:
        dbClose()
        omAssert(None, msg='book with id {} not found'.format(book_id))
    except Session.DoesNotExist:
        dbClose()
        omAssert(None, msg='token {} not found'.format(token))
    except BookUser.DoesNotExist:
        name = user.name
        dbClose()
        omAssert(None, msg='caller {} not in book id {}'.format(name, book_id))

def addTransaction(token, book_id, user_from, user_to, amount):
    """
    [IN]:
        (str)token
        (int)book_id
        (str)user_from
        (str)user_to
        (double)amount
    [OUT]:
        (int)id
        (int)book_id
        (str)user_from
        (str)user_to
        (double)amount
        (str)create_time
    """
    dbOpen()

    try:
        # get caller and book context
        caller = Session.get(Session.token == token).user
        book = Book.get(Book.id == book_id)
        bookuser_caller = BookUser.get(BookUser.book == book, BookUser.user == caller)

        # check user_from and his presence in book
        try:
            user_from = User.get(User.name == user_from)
            bookuser_from = BookUser.get(BookUser.book == book, BookUser.user == user_from)
        except User.DoesNotExist:
            dbClose()
            omAssert(None, msg='user_from {} not found'.format(user_from))
        except BookUser.DoesNotExist:
            dbClose()
            omAssert(None, msg='user_from is not in book id {}:{}'.format(user_from, book_id, book.name))

        # check user_to and his presence in book
        try:
            user_to = User.get(User.name == user_to)
            bookuser_to = BookUser.get(BookUser.book == book, BookUser.user == user_to)
        except User.DoesNotExist:
            dbClose()
            omAssert(None, msg='user_to {} not found'.format(user_to))
        except BookUser.DoesNotExist:
            dbClose()
            omAssert(None, msg='user_to is not in book id {}:{}'.format(user_to, book_id, book.name))

        # permit conditions
        cond1 = bookuser_caller.is_admin
        cond2 = caller.id == user_from.id
        if cond1 or cond2:
            transaction = Transaction(book=book, user_from=user_from, user_to=user_to, amount=amount)
            transaction.save()

            dbClose()
            return {
                'id': transaction.id,
                'book_id': book.id,
                'user_from': user_from.name,
                'user_to': user_to.name,
                'amount': amount,
                'create_time': str(transaction.create_time),
            }

        # access denied
        else:
            dbClose()
            omAssert(None, msg='access denied')

    # If ... not found - Raise exception
    except Book.DoesNotExist:
        dbClose()
        omAssert(None, msg='book with id {} not found'.format(book_id))
    except BookUser.DoesNotExist:
        dbClose()
        omAssert(None, msg='caller {} is not in book id {}:{}'.format(caller.name, book_id, book.name))
    except Session.DoesNotExist:
        dbClose()
        omAssert(None, msg='token {} not found'.format(token))

def removeTransaction(token, transaction_id):
    """
    [IN]:
        (str)token
        (int)transaction_id
    """
    dbOpen()

    try:
        # get caller and transaction contexts
        caller = Session.get(Session.token == token).user
        transaction = Transaction.get(Transaction.id == transaction_id)
        bookuser_caller = BookUser.get(BookUser.book == transaction.book, BookUser.user == caller)

        # permit conditions
        cond1 = bookuser_caller.is_admin
        cond2 = caller.id == transaction.user_from.id
        if cond1 or cond2:
            transaction.delete_instance()
            dbClose()
            return

        # access denied
        else:
            dbClose()
            omAssert(None, msg='access denied')

    # If ... found - Raise exception
    except BookUser.DoesNotExist:
        dbClose()
        omAssert(None, msg='caller {} is not in book id {}:{}'.format(caller.name, transaction.book.id, transaction.book.name))
    except Session.DoesNotExist:
        dbClose()
        omAssert(None, msg='token {} not found'.format(token))
    except Transaction.DoesNotExist:
        dbClose()
        omAssert(None, msg='transaction id {} not found'.format(transaction_id))

def simplify(token, book_id):
    """
    [IN]:
        (str)token
        (int)book_id
    [OUT]:
        (transactions) [{'from': ..., 'to': ..., 'amount': ...}, ...]
    """
    dbOpen()

    try:
        # check session
        session = Session.get(Session.token == token)
        # get user
        user = session.user
        # get book
        book = Book.get(Book.id == book_id)
        # check that user is in book
        BookUser.get(BookUser.book == book, BookUser.user == user)

        # build db query to get all book transaction
        query_trans = Transaction.select().where(Transaction.book == book)

        # build specific object for cimplification
        transactions = [{'from': trans.user_from.name, 'to': trans.user_to.name,
                        'amount': trans.amount} for trans in query_trans]
        
        # close db. dont need anymore
        dbClose()

        # simplify and return
        return simplifier.simplify(transactions)

    # If ... found - Raise exception
    except Book.DoesNotExist:
        dbClose()
        omAssert(None, msg='book id {} not found'.format(book_id))
    except BookUser.DoesNotExist:
        dbClose()
        omAssert(None, msg='caller {} is not in book {}:{}'.format(user.name, book.id, book.name))
    except Session.DoesNotExist:
        dbClose()
        omAssert(None, msg='token {} not found'.format(token))
    except Transaction.DoesNotExist:
        dbClose()
        omAssert(None, msg='transaction id {} not found'.format(transaction_id))
