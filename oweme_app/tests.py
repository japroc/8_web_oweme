# Change db
import .settings
settings.DB_NAME = settings.TEST_DB_NAME

import os
import json
import time
import peewee as pw

from .oweme import do
from .omdbtables import db, dbOpen, dbClose
from .omdbtables import MODELS, User, Session, BookUser, Book, Transaction
from . import omglobals
from .omglobals import ERROR_SUCCESS, ERROR_ERROR

# ----------
# Exception
# ----------

class TestException(Exception):
    def __init__(self, message):
        # Call the base class constructor with the parameters it needs
        super(TestException, self).__init__(message)

def testIt(req, bad_res):
    res = json.loads(do(json.dumps(req)))
    if res.get('status') == bad_res:
        if bad_res == ERROR_ERROR:
            print(res.get('message'))
        raise TestException(str(req))
    return res

def testRegistration(test_idx):
    u1 = 'u1'
    u2 = 'u2'
    bad_passwd = 'abcde'
    passwd = 'abcdef'

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':u1, 'password':bad_passwd}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_REGISTRATION, 'password':'abcde'}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':u1}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':u1, 'password':passwd}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':u1, 'password':passwd}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':u2, 'password':passwd}
    testIt(req, ERROR_ERROR)

    dbOpen()
    if User.select().count() != 2:
        dbClose()
        raise TestException('User.select().count() != 2')
    dbClose()

    return True

def testLogin(test_idx):
    username = 'username'
    password = 'password'
    wrong_password = 'wrong_password'
    short_password = 'short'

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_LOGIN, 'username':username, 'password':short_password}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_LOGIN, 'username':username, 'password':wrong_password}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_LOGIN, 'username':username, 'password':password}
    testIt(req, ERROR_ERROR)

    dbOpen()
    user = User.select().where(User.name == username)
    sessions = Session.select().where(Session.user.in_(user))
    if sessions.count() != 1:
        res = False
    else:
        print('[TESTS][{}] Token: {}'.format(test_idx, sessions[0].token))
        res = True
    dbClose()

    return res

def testLogout(test_idx):
    username = 'username'
    password = 'password'

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_LOGIN, 'username':username, 'password':password}
    res = testIt(req, ERROR_ERROR)

    token = res['result']['token']

    req = {'action':omglobals.ACTION_LOGOUT}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_LOGOUT, 'token':''}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_LOGOUT, 'token':token}
    testIt(req, ERROR_ERROR)

    dbOpen()
    if Session.select().count() != 0:
        dbClose()
        raise TestException('Session.select().count() != 0')
    dbClose()

    return True

def testCreateBook(test_idx):
    username = 'username'
    password = 'password'
    bookname = 'bookname'

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_LOGIN, 'username':username, 'password':password}
    res = testIt(req, ERROR_ERROR)

    token = res['result']['token']

    req = {'action':omglobals.ACTION_CREATE_BOOK, 'token':token, 'name':bookname}
    testIt(req, ERROR_ERROR)

    dbOpen()
    res = False
    try:
        user = User.get(User.name == username)
        bookuser = BookUser.get(BookUser.user == user)
        if bookuser.is_admin == True:
            # print('[TESTS][{}] {}'.format(test_idx, str(res)))
            res = True
    except pw.DoesNotExist:
        pass
    dbClose()

    return True

def testDeleteBook(test_idx):
    username1 = 'username1'
    username2 = 'username2'
    password = 'password'
    bookname = 'bookname'

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username1, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username2, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_LOGIN, 'username':username1, 'password':password}
    res = testIt(req, ERROR_ERROR)
    token1 = res['result']['token']

    req = {'action':omglobals.ACTION_LOGIN, 'username':username2, 'password':password}
    res = testIt(req, ERROR_ERROR)
    token2 = res['result']['token']

    req = {'action':omglobals.ACTION_CREATE_BOOK, 'token':token1, 'name':bookname}
    res = testIt(req, ERROR_ERROR)
    book_id = res['result']['id']

    dbOpen()
    user2 = User.get(User.name == username2)
    book = Book.get(Book.name == bookname)
    bookuser = BookUser(user=user2, book=book, is_admin=False)
    bookuser.save()
    dbClose()

    req = {'action':omglobals.ACTION_DELETE_BOOK, 'token':token2, 'id':book_id}
    testIt(req, ERROR_SUCCESS)

    dbOpen()
    user2 = User.get(User.name == username2)
    book = Book.get(Book.name == bookname)
    bookuser = BookUser.get(user=user2, book=book)
    bookuser.is_admin = True
    bookuser.save()
    dbClose()

    req = {'action':omglobals.ACTION_DELETE_BOOK, 'token':token2, 'id':book_id}
    testIt(req, ERROR_ERROR)

    dbOpen()
    if BookUser.select().count() > 0:
        dbClose()
        raise TestException('BookUser.select().count() > 0')
    if Book.select().count() > 0:
        dbClose()
        raise TestException('Book.select().count() > 0')
    dbClose()

    req = {'action':omglobals.ACTION_CREATE_BOOK, 'token':token1, 'name':bookname}
    res = testIt(req, ERROR_ERROR)
    book_id = res['result']['id']

    req = {'action':omglobals.ACTION_DELETE_BOOK, 'token':token1, 'id':book_id}
    testIt(req, ERROR_ERROR)

    return True

def testAddUserToBook(test_idx):
    username1 = 'username1'
    username2 = 'username2'
    password = 'password'
    bookname = 'bookname'

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username1, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username2, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_LOGIN, 'username':username1, 'password':password}
    res = testIt(req, ERROR_ERROR)
    token1 = res['result']['token']

    req = {'action':omglobals.ACTION_CREATE_BOOK, 'token':token1, 'name':bookname}
    res = testIt(req, ERROR_ERROR)
    book_id = res['result']['id']

    req = {'action':omglobals.ACTION_ADD_USER_TO_BOOK, 'token':token1, 'book_id':book_id, 'username':username2}
    testIt(req, ERROR_ERROR)

    dbOpen()
    if BookUser.select().count() != 2:
        dbClose()
        raise TestException('BookUser.select().count() > 0')
    user = User.get(User.name == username2)
    book = Book.get(Book.id == book_id)
    bookuser = BookUser.get(BookUser.user == user, BookUser.book == book)
    if bookuser.is_admin is True:
        dbClose()
        raise TestException('bookuser.is_admin is True')

    dbClose()
    return True

def testUpgradeUser(test_idx):
    username1 = 'username1'
    username2 = 'username2'
    username3 = 'username3'
    password = 'password'
    bookname = 'bookname'

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username1, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username2, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username3, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_LOGIN, 'username':username1, 'password':password}
    res = testIt(req, ERROR_ERROR)
    token1 = res['result']['token']
    print('[TESTS][{}] token1: {}'.format(test_idx, token1))

    req = {'action':omglobals.ACTION_LOGIN, 'username':username2, 'password':password}
    res = testIt(req, ERROR_ERROR)
    token2 = res['result']['token']
    print('[TESTS][{}] token2: {}'.format(test_idx, token2))

    req = {'action':omglobals.ACTION_CREATE_BOOK, 'token':token1, 'name':bookname}
    res = testIt(req, ERROR_ERROR)
    book_id = res['result']['id']
    print('[TESTS][{}] book_id: {}'.format(test_idx, book_id))

    req = {'action':omglobals.ACTION_ADD_USER_TO_BOOK, 'token':token1, 'book_id':book_id, 'username':username2}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_ADD_USER_TO_BOOK, 'token':token2, 'book_id':book_id, 'username':username3}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_ADD_USER_TO_BOOK, 'token':token2, 'book_id':book_id, 'username':username2}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_UPGRADE_USER, 'token':token2, 'book_id':book_id, 'username':username2}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_UPGRADE_USER, 'token':token2, 'book_id':book_id, 'username':username3}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_UPGRADE_USER, 'token':token1, 'book_id':book_id, 'username':username2}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_ADD_USER_TO_BOOK, 'token':token2, 'book_id':book_id, 'username':username3}
    testIt(req, ERROR_ERROR)

    return True

def testDowngradeUser(test_idx):
    username1 = 'username1'
    username2 = 'username2'
    username3 = 'username3'
    password = 'password'
    bookname = 'bookname'

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username1, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username2, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username3, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_LOGIN, 'username':username1, 'password':password}
    res = testIt(req, ERROR_ERROR)
    token1 = res['result']['token']
    print('[TESTS][{}] token1: {}'.format(test_idx, token1))

    req = {'action':omglobals.ACTION_LOGIN, 'username':username2, 'password':password}
    res = testIt(req, ERROR_ERROR)
    token2 = res['result']['token']
    print('[TESTS][{}] token2: {}'.format(test_idx, token2))

    req = {'action':omglobals.ACTION_CREATE_BOOK, 'token':token1, 'name':bookname}
    res = testIt(req, ERROR_ERROR)
    book_id = res['result']['id']
    print('[TESTS][{}] book_id: {}'.format(test_idx, book_id))
    
    req = {'action':omglobals.ACTION_DOWNGRADE_USER, 'token':token1, 'book_id':book_id, 'username':username3}
    testIt(req, ERROR_SUCCESS)
    
    req = {'action':omglobals.ACTION_UPGRADE_USER, 'token':token1, 'book_id':book_id, 'username':username3}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_DOWNGRADE_USER, 'token':token2, 'book_id':book_id, 'username':username3}
    testIt(req, ERROR_SUCCESS)
    
    req = {'action':omglobals.ACTION_UPGRADE_USER, 'token':token2, 'book_id':book_id, 'username':username3}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_ADD_USER_TO_BOOK, 'token':token1, 'book_id':book_id, 'username':username2}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_ADD_USER_TO_BOOK, 'token':token2, 'book_id':book_id, 'username':username3}
    testIt(req, ERROR_SUCCESS)
    
    req = {'action':omglobals.ACTION_UPGRADE_USER, 'token':token1, 'book_id':book_id, 'username':username2}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_ADD_USER_TO_BOOK, 'token':token2, 'book_id':book_id, 'username':username3}
    testIt(req, ERROR_ERROR)
    
    req = {'action':omglobals.ACTION_DOWNGRADE_USER, 'token':token1, 'book_id':book_id, 'username':username3}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_DOWNGRADE_USER, 'token':token2, 'book_id':book_id, 'username':username3}
    testIt(req, ERROR_SUCCESS)
    
    req = {'action':omglobals.ACTION_UPGRADE_USER, 'token':token1, 'book_id':book_id, 'username':username3}
    testIt(req, ERROR_ERROR)
    
    req = {'action':omglobals.ACTION_DOWNGRADE_USER, 'token':token1, 'book_id':book_id, 'username':username3}
    testIt(req, ERROR_ERROR)
    
    req = {'action':omglobals.ACTION_UPGRADE_USER, 'token':token1, 'book_id':book_id, 'username':username3}
    testIt(req, ERROR_ERROR)
    
    req = {'action':omglobals.ACTION_DOWNGRADE_USER, 'token':token2, 'book_id':book_id, 'username':username3}
    testIt(req, ERROR_ERROR)

    return True

def testListBooks(test_idx):
    username1 = 'username1'
    username2 = 'username2'
    password = 'password'
    bookname1 = 'bookname1'
    bookname2 = 'bookname2'

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username1, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username2, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_LOGIN, 'username':username1, 'password':password}
    res = testIt(req, ERROR_ERROR)
    token1 = res['result']['token']
    print('[TESTS][{}] token1: {}'.format(test_idx, token1))

    req = {'action':omglobals.ACTION_LOGIN, 'username':username2, 'password':password}
    res = testIt(req, ERROR_ERROR)
    token2 = res['result']['token']
    print('[TESTS][{}] token2: {}'.format(test_idx, token2))

    req = {'action':omglobals.ACTION_CREATE_BOOK, 'token':token1, 'name':bookname1}
    res = testIt(req, ERROR_ERROR)
    book_id1 = res['result']['id']
    print('[TESTS][{}] book_id1: {}'.format(test_idx, book_id1))

    req = {'action':omglobals.ACTION_CREATE_BOOK, 'token':token2, 'name':bookname2}
    res = testIt(req, ERROR_ERROR)
    book_id2 = res['result']['id']
    print('[TESTS][{}] book_id2: {}'.format(test_idx, book_id2))

    req = {'action':omglobals.ACTION_ADD_USER_TO_BOOK, 'token':token2, 'book_id':book_id2, 'username':username1}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_LIST_BOOKS, 'token':token1}
    res = testIt(req, ERROR_ERROR)
    books = res['result']
    print('[TESTS][{}] books: {}'.format(test_idx, books))

    if books != [{'id':book_id1, 'name':bookname1, 'owner':username1}, {'id':book_id2, 'name':bookname2, 'owner':username2}]:
        ex_str = "books != [{'id':book_id1, 'name':bookname1, 'owner':username1}, "
        ex_str += "{'id':book_id2s, 'name':bookname2s, 'owner':username2s}]"
        raise TestException(ex_str)

    return True

def testGetBookInfo(test_idx):
    username1 = 'username1'
    username2 = 'username2'
    password = 'password'
    bookname = 'bookname1'

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username1, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username2, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_LOGIN, 'username':username1, 'password':password}
    res = testIt(req, ERROR_ERROR)
    token1 = res['result']['token']
    print('[TESTS][{}] token1: {}'.format(test_idx, token1))

    req = {'action':omglobals.ACTION_LOGIN, 'username':username2, 'password':password}
    res = testIt(req, ERROR_ERROR)
    token2 = res['result']['token']
    print('[TESTS][{}] token2: {}'.format(test_idx, token2))

    req = {'action':omglobals.ACTION_CREATE_BOOK, 'token':token1, 'name':bookname}
    res = testIt(req, ERROR_ERROR)
    book_id = res['result']['id']
    print('[TESTS][{}] book_id: {}'.format(test_idx, book_id))

    req = {'action':omglobals.ACTION_ADD_USER_TO_BOOK, 'token':token1, 'book_id':book_id, 'username':username2}
    testIt(req, ERROR_ERROR)

    excpected = {
        'id':book_id, 'name':bookname, 'owner':username1, 
        'users':[
            {'name':username1, 'is_admin':True},
            {'name':username2, 'is_admin':False}
        ], 
        'transactions':[]
    }

    req = {'action':omglobals.ACTION_GET_BOOK_INFO, 'token':token1, 'book_id':book_id}
    res = testIt(req, ERROR_ERROR)
    book_info = res['result']
    print('[TESTS][{}] book_info: {}'.format(test_idx, book_info))

    if book_info != excpected:
        print('excpected: {}'.format(excpected))
        raise TestException('book_info != excpected')

    req = {'action':omglobals.ACTION_GET_BOOK_INFO, 'token':token2, 'book_id':book_id}
    res = testIt(req, ERROR_ERROR)
    book_info = res['result']
    print('[TESTS][{}] book_info: {}'.format(test_idx, book_info))

    if book_info != excpected:
        print('excpected: {}'.format(excpected))
        raise TestException('book_info != excpected')

    return True

def testAddRemoveTransaction(test_idx):
    username1 = 'username1'
    username2 = 'username2'
    username3 = 'username3'
    password = 'password'
    bookname = 'bookname1'
    bookname = 'bookname2'

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username1, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username2, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_REGISTRATION, 'username':username3, 'password':password}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_LOGIN, 'username':username1, 'password':password}
    res = testIt(req, ERROR_ERROR)
    token1 = res['result']['token']
    print('[TESTS][{}] token1: {}'.format(test_idx, token1))

    req = {'action':omglobals.ACTION_LOGIN, 'username':username2, 'password':password}
    res = testIt(req, ERROR_ERROR)
    token2 = res['result']['token']
    print('[TESTS][{}] token2: {}'.format(test_idx, token2))

    req = {'action':omglobals.ACTION_LOGIN, 'username':username3, 'password':password}
    res = testIt(req, ERROR_ERROR)
    token3 = res['result']['token']
    print('[TESTS][{}] token3: {}'.format(test_idx, token3))

    req = {'action':omglobals.ACTION_CREATE_BOOK, 'token':token1, 'name':bookname}
    res = testIt(req, ERROR_ERROR)
    book_id1 = res['result']['id']
    print('[TESTS][{}] book_id1: {}'.format(test_idx, book_id1))

    req = {'action':omglobals.ACTION_CREATE_BOOK, 'token':token2, 'name':bookname}
    res = testIt(req, ERROR_ERROR)
    book_id2 = res['result']['id']
    print('[TESTS][{}] book_id2: {}'.format(test_idx, book_id2))

    req = {'action':omglobals.ACTION_ADD_USER_TO_BOOK, 'token':token1, 'book_id':book_id1, 'username':username3}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_ADD_USER_TO_BOOK, 'token':token2, 'book_id':book_id2, 'username':username3}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_ADD_TRANSACTION, 'token':token1, 'book_id':book_id1,
            'user_from':username1, 'user_to':username3, 'amount':''}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_ADD_TRANSACTION, 'token':token1, 'book_id':book_id1,
            'user_from':username1, 'user_to':username3, 'amount':0.001}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_ADD_TRANSACTION, 'token':token1, 'book_id':book_id2,
            'user_from':username2, 'user_to':username3, 'amount':1}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_ADD_TRANSACTION, 'token':token1, 'book_id':book_id2,
            'user_from':username3, 'user_to':username2, 'amount':1}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_ADD_TRANSACTION, 'token':token1, 'book_id':book_id1,
            'user_from':username3, 'user_to':username1, 'amount':1}
    res = testIt(req, ERROR_ERROR)
    trans_id311 = res['result']['id']

    req = {'action':omglobals.ACTION_ADD_TRANSACTION, 'token':token1, 'book_id':book_id1,
            'user_from':username1, 'user_to':username3, 'amount':1}
    res = testIt(req, ERROR_ERROR)
    trans_id13 = res['result']['id']

    req = {'action':omglobals.ACTION_ADD_TRANSACTION, 'token':token3, 'book_id':book_id1,
            'user_from':username1, 'user_to':username3, 'amount':1}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_ADD_TRANSACTION, 'token':token3, 'book_id':book_id1,
            'user_from':username3, 'user_to':username1, 'amount':1}
    res = testIt(req, ERROR_ERROR)
    trans_id312 = res['result']['id']

    req = {'action':omglobals.ACTION_REMOVE_TRANSACTION, 'token':token2}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_REMOVE_TRANSACTION, 'token':token2, 'transaction_id':''}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_REMOVE_TRANSACTION, 'token':token2, 'transaction_id':999}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_REMOVE_TRANSACTION, 'token':token2, 'transaction_id':trans_id311}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_REMOVE_TRANSACTION, 'token':token3, 'transaction_id':trans_id13}
    testIt(req, ERROR_SUCCESS)

    req = {'action':omglobals.ACTION_REMOVE_TRANSACTION, 'token':token1, 'transaction_id':trans_id13}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_REMOVE_TRANSACTION, 'token':token3, 'transaction_id':trans_id311}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_REMOVE_TRANSACTION, 'token':token1, 'transaction_id':trans_id312}
    testIt(req, ERROR_ERROR)

    req = {'action':omglobals.ACTION_REMOVE_TRANSACTION, 'token':token1, 'transaction_id':trans_id312}
    testIt(req, ERROR_SUCCESS)

    return True

""" Add new tests to TESTS list """
TESTS = [
    testAddRemoveTransaction,
    testGetBookInfo, testListBooks,
    testDowngradeUser, testUpgradeUser, testAddUserToBook, 
    testDeleteBook, testCreateBook,
    testRegistration, testLogin, testLogout,
]

""" main """

def main():
    """
    Tests must return True on success, False otherwise
    """
    print('[TESTS] Start...')
    print('[TESTS] Total tests: {}'.format(len(TESTS)))

    success = 0
    test_idx = 1

    if os.path.isfile(settings.DB_NAME):
        os.remove(settings.DB_NAME)

    for test in TESTS:
        print('[TESTS][{}] Test: {}.'.format(test_idx, test.__name__))

        try:
            if test(test_idx) is True:
                success += 1
                print('[TESTS][{}] GOOD.'.format(test_idx))
            else:
                print('[TESTS][{}] BAD.'.format(test_idx))
        except TestException as e:
            print('[TESTS][{}][Exception] {}'.format(test_idx, e))
            print('[TESTS][{}] BAD.'.format(test_idx))


        if os.path.isfile(settings.DB_NAME):
            os.remove(settings.DB_NAME)

        test_idx += 1

    print('[TESTS] Success/Total {}/{}'.format(success, len(TESTS)))
    print('[TESTS] Finished.')

if __name__ == "__main__":
    main()
