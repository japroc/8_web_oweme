## Installation
1. Install Python (Python3 tested)
2. `git clone https://gitlab.com/japroc/8_web_oweme`
3. `pip install -r requirements.txt`

## Deploying
1. [How To Serve Flask Applications with uWSGI and Nginx on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uwsgi-and-nginx-on-ubuntu-16-04)
2. [How To Create a Self-Signed SSL Certificate for Nginx in Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-nginx-in-ubuntu-16-04)
3. [HTTPS NGINX and self-signed Certificates](https://stackoverflow.com/questions/3761737/https-get-ssl-with-android-and-self-signed-server-certificate)

* [OpenSSL Essentials: Working with SSL Certificates, Private Keys and CSRs](https://www.digitalocean.com/community/tutorials/openssl-essentials-working-with-ssl-certificates-private-keys-and-csrs)
* [How To Secure Nginx with Let's Encrypt on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-16-04)

## Run
##### Native over socket
```
python native_server.py
```
##### Flask
```
python flask_app_server.py
```
##### uwsgi
```
uwsgi --socket 0.0.0.0:5000 --protocol=http -w wsgi:app
```
##### Tests
```
python tests.py
```

## API
##### Auth
* registration(username, password)
* login(username, password) -> token
* logout(token)

##### Book
* createbook(token, name) -> book_id, name, owner, create_time
* deletebook(token, book_id)
* addusertobook(token, book_id, username)
* upgradeuser(token, book_id, username)
* downgradeuser(token, book_id, username)
* listbooks(token) -> [{id, name, owner}, ...]
* getbookinfo(token, bid) -> {'id':1, 'name':'bookname', 'owner':'user1', 'users':[{'name':user1, 'is_admin':True}, {'name':user2, 'is_admin':False}, ...], 'transactions':[]}

##### Transactions
* addtransaction(token, book_id, user_from, user_to, amount) -> {'id': 1, 'book_id': 1, 'user_from':'user1', 'user_to':'user2', 'amount':'100.50, 'create_time':...}
* removetransaction(token, transaction_id)

## ToDoApi
##### Book
* removeuserfrombook(token, book_id, username)

##### Transactions
* addfairtransaction(from_username, to_usernames_list[], money)
* addcheck(from_tomoney_list[])

##### Friends
* addfriend(username)
* removefriend(username)
* listfriends() -> [uid1, uid2, ...]

## TODO
* Какой алгоритм использеут bcrypt?
* Не отправлять сенсатив дата в открытом виде
* Вертска кнопок, минимальный размер поддерживаемого экрана
* Веб сервак + вся безопасность, сканить
