import socket
from oweme_app import oweme
from oweme_app.settings import TCP_IP, TCP_PORT, BUFFER_SIZE

def fullRecv(conn):
    msg = b''
    conn.settimeout(2)
    while 1:
        try:
            data = conn.recv(1)
            msg += data
        except socket.timeout as e:
            if len(msg) != 0:
                break
    return msg.decode('utf-8')

def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((TCP_IP, TCP_PORT))
    s.listen(1)
    s.settimeout(2)

    print('[Server] Started to listen on {}:{}...'.format(TCP_IP, TCP_PORT))
    print('Listening...')

    while True:

        try:
            conn, addr = s.accept()
        except socket.timeout as e:
            continue
        except Exception as e:
            raise e

        print('[+] ' + '-' * 50)
        print('[+] Connection address: ', addr)
        req = fullRecv(conn)
        print('[>] Total Received (len: {}): {}'.format(len(req), req))
        response = oweme.handleRequest(req)
        print('[<] Response: {}'.format(response))
        conn.send(response.encode())
        conn.close()
        print('[-] Closed Connection: ', addr)
        print('[+] ' + '-' * 50)

if __name__ == "__main__":
    main()
    